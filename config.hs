import Propellor
import Propellor.Property.DiskImage
import Propellor.Property.Chroot
import Propellor.Property.Bootstrap
import Propellor.Property.Versioned
import Propellor.Property.Parted
import Propellor.Property.Installer
import qualified Propellor.Property.File as File
import qualified Propellor.Property.Apt as Apt
import qualified Propellor.Property.Hostname as Hostname
import qualified Propellor.Property.Locale as Locale
import qualified Propellor.Property.Grub as Grub
import qualified Propellor.Property.XFCE as XFCE
import Installer.Types
import Installer.Main
import Installer.User
import Installer.Server
import Installer.UserInput

main :: IO ()
main = installerMain hosts

hosts :: [Host]
hosts =
	[ installer
	, installer_builder
	, darkstar
	]

-- | This is not a complete Host definition; it can be used on any host
-- to build the installer disk images, by running, as root:
-- 	propellor installer.builder
installer_builder :: Host
installer_builder = host "installer.builder" $ props
	& bootstrapWith (Robustly Stack) -- temporary
	& osDebian Unstable X86_64
	& installerBuilt

-- | An example host that propellor runs on and builds the installer
-- disk images. This is an alternate way to build the disk images,
-- and lets you run
-- 	propellor --spin darkstar.kitenet.net
-- (Replace darkstar with your own hostname.)
darkstar :: Host
darkstar = host "darkstar.kitenet.net" $ props
	& bootstrapWith (Robustly Stack) -- temporary
	& osDebian Unstable X86_64
	& installerBuilt
		`before` File.ownerGroup "/srv/installer.img" (User "joey") (Group "joey")
		`before` File.ownerGroup "/srv/installer.vmdk" (User "joey") (Group "joey")

-- | Build a disk image for the installer.
installerBuilt :: RevertableProperty (HasInfo + DebianLike) Linux
installerBuilt = imageBuilt (VirtualBoxPointer "/srv/installer.vmdk")
	(hostChroot installer (Debootstrapped mempty))
	MSDOS
	[ partition EXT4 `mountedAt` "/"
		`setFlag` BootFlag
		`mountOpt` errorReadonly
		`reservedSpacePercentage` 0
		`addFreeSpace` MegaBytes 256
	]

data Variety = Installer | Target
	deriving (Eq)

installer :: Host
installer = seed `version` Installer

-- | The seed of a Debian system. This comes in two varieties;
-- the Installer boots to a default user and displays a user interface.
-- When the user chooses to install to a disk, the Target system
-- is built, with their input available in userInput.
--
-- Defining both varieties in the same place like this allows
-- the Installer to be copied to the disk, and then a minimum of work done
-- to convert that to the Target system.
seed :: Versioned Variety Host
seed ver = host "debian.local" $ props
	& osDebian Unstable X86_64
	& Hostname.sane
	& Apt.stdSourcesList
	& Apt.installed ["linux-image-amd64"]
	& Grub.installed PC
	& XFCE.installed
	& Apt.installed ["firefox"]
	& "en_US.UTF-8" `Locale.selectedFor` ["LANG"]

	& bootstrapWith (Robustly Stack) -- temporary
	& bootstrappedFrom GitRepoOutsideChroot

	& ver (   (== Installer) --> installerUser
	      <|> (== Target)    --> desktopUser (inputUserName userInput)
	      )
	& ver (   (== Installer) --> autostartInstaller)
	& ver (   (== Installer) --> targetInstalled seed Target userInput parts)
	& ver (   (== Target)    --> fstabLists userInput parts)
	& ver (   (== Installer) --> targetBootable userInput)
  where
	parts = TargetPartTable MSDOS
		[ partition EXT2 `mountedAt` "/boot"
			`setFlag` BootFlag
			`mountOpt` errorReadonly
			`setSize` MegaBytes 512
		, partition EXT4 `mountedAt` "/"
			`mountOpt` errorReadonly
			`useDiskSpace` RemainingSpace
		, swapPartition (MegaBytes 1024)
		]
