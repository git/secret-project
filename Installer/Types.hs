module Installer.Types where

import qualified Propellor.Property.Installer.Types as P

newtype UserName = UserName String
	deriving (Read, Show)

-- | This is serialized out to the Installer.UserInput module,
-- so should not contain passwords or other sensative information.
-- (Store such information in propellor PrivData instead.)
data UserInput = UserInput
	{ _targetDiskDevice :: Maybe P.TargetDiskDevice
	, _diskEraseConfirmed :: Maybe P.DiskEraseConfirmed
	, inputUserName :: Maybe UserName
	}
	deriving (Read, Show)

instance P.UserInput UserInput where
	targetDiskDevice = _targetDiskDevice
	diskEraseConfirmed = _diskEraseConfirmed

noUserInput :: UserInput
noUserInput = UserInput
	{ _targetDiskDevice = Nothing
	, _diskEraseConfirmed = Nothing
	, inputUserName = Nothing
	}
